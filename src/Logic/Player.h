#ifndef ZCPP_MISSIONIMPOSSIBLE_PLAYER_H
#define ZCPP_MISSIONIMPOSSIBLE_PLAYER_H


#include "Person.h"

/*!
 * @brief Obiekt tej klasy reprezentuje postać którą steruje gracz
 */
class Player : public Person{
public:
    /*!
     * @brief Wykorzystywany konstruktor
     */
    Player();
};


#endif //ZCPP_MISSIONIMPOSSIBLE_PLAYER_H
